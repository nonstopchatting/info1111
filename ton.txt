Amazon started as a bookseller without any extra products but over the last decade, it has branched out into additional product areas and the third-party sales that now represent a good chunk of its revenue (some estimates put it at 25 percent). Although Amazon has a broad mandate, the largest component of it is still electronic commerce which take up a big amount of operating posts. 
Here are some basic skills which are required in working in Amazon:
Strong work values �� Dependability, honesty, selfconfidence and a positive attitude are prized qualities in any profession. Employers look for personal integrity.
Communication skills �� Listening, speaking and writing. Employers want people who can accurately interpret what others are saying and organize and express their thoughts clearly.
Teamwork �� In today��s work environment, many jobs involve working in one or more groups. Employers want someone who can bring out the best in others.
Analytical and problem-solving skills �� Employers want people who can use creativity, reasoning and past experiences to identify and solve problems effectively.








Information System

(Information Systems is a dynamic career field focused on the employment of various information technologies in ways that help achieve the strategic goals and operational objectives of businesses or other organizations. Information technologies have increasingly become woven into the fabric of the most value adding processes in organizations, creating a demand for skilled individuals who can think in terms of the company��s business, technology, and organizational or ��people�� strategies in a way that ensures each is aligned with and reinforcing of the other two.)
Opportunities for employment in the information systems/information technology career field are many and varied. Virtually every organization, businesses in all industries, government entities, or not-for-profit groups, depends to some degree on information technology and, as a result, adds to the aggregate demand for information systems professionals. Some positions are more technical than analytical, and some organizations choose to place entry level employees in these sorts of positions to ��learn the ropes.��
For Amazon, system had collected tons of consumer's information and their favors which can be used by further business. Under this circumstances, skillfully usage of the IT technique is the fundamental basis moreover the quantity of the package of information from services will keep growing.   
The capacities below are recommended for better performance in working in Amazon:
Database skills --- disposing a large amount of information database
Programming --- structuring cloud and serves
System automation --- storing and employing information automatically 
Information security --- ensuring property safety, identity, interviewing and software development security
Project management

Here are some departments which require the attendance of new graduates: 
Amazon Web Service - support developers with direct access to Amazon's strong technology platform.
Simple Storage Service - design to make web-scale computing easier by providing web service interface that can be used to store and retrieve any amount of data, at any time, from anywhere on the web.
Internet-based customer referral system �C provide the most suitable and humanized recommended products. 
Integrating transaction mechanism over multiple internet sites �C for convenient and safe operations

























How Amazon Works (Layton). Retrieved from https://money.howstuffworks.com/amazon2.htm
Top 10 Employability Skills. Retrieved from http://www.opportunityjobnetwork.com/job-resources/help/top-10-skills.html
These 9 Cloud Computing Skills. Albright(2015,July,16). Retrieved from https://www.makeuseof.com/tag/9-cloud-computing-skills-give-next-career-jump/
The University of Kansas. Retrieved from https://business.ku.edu/what-can-i-do-major-information-systems
Use of Management Information Systems at Amazon. Fu(2013,August,20) Retrieved from https://prezi.com/02oqodq-dra7/use-of-management-information-systems-at-amazon/

